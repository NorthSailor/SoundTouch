#-------------------------------------------------
#
# Project created by QtCreator 2016-02-12T16:31:35
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SoundTouch
TEMPLATE = app

CONFIG += espeak c++14

SOURCES += main.cpp\
    view.cpp \
    node.cpp \
    button.cpp \
    gridlayout.cpp \
    optionnode.cpp \
    switchnode.cpp \
    filelayout.cpp \
    launcherlayout.cpp

HEADERS  += \
    view.h \
    layout.h \
    node.h \
    button.h \
    gridlayout.h \
    optionnode.h \
    switchnode.h \
    filelayout.h \
    launcherlayout.h

espeak {
        LIBS += -lespeak
        DEFINES += USE_ESPEAK
}
