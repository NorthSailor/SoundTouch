#ifndef GRIDLAYOUT_H
#define GRIDLAYOUT_H

#include "layout.h"
#include <QVector>

class Node;

class GridLayout : public Layout
{
    Q_OBJECT
public:
	GridLayout(int rows, int columns);

    virtual void handleMouseEvent(QMouseEvent *e);
    virtual void handleKeyEvent(QKeyEvent *e);
    virtual void handleWheelEvent(QWheelEvent *e);
    virtual void paint(QPainter *p);

    ///
    /// \brief Provides access to the labels vector.
    /// \return A mutable reference to the labels vector.
    ///
    /// Label names are stored from first to last.
    /// Thus the name for the item at the second row and third column
    /// of a 4 column grid will be at (2 * 4 + 3).
    ///
	QVector<Node*> &nodes() { return _nodes; }

    ///
    /// \brief Changes the size of the grid.
    /// \param rows The new number of rows.
    /// \param columns The new number of columns.
    ///
    /// The labels vector is resized. Any new items are filled with placeholder
    /// text. Excess items are deleted. Schedules a repaint.
    /// The new active cell is at (0, 0)
    ///
    virtual void resize(int rows, int columns);

signals:

public slots:

protected:
    int _rows, _columns, _fixedItems;
    int _activeRow, _activeColumn;
    int _rectWidth, _rectHeight;

	QVector<Node*> _nodes;

    /// Moves the selection to a new element.
    void moveTo(int row, int column);

    /// Returns the label of the item at the given row and column.
    QString textAt(int row, int column);

	/// Returns a pointer to the node at the given location or nullptr
	/// if no node has been set.
	Node *nodeAt(int row, int column);
};

#endif // GRIDLAYOUT_H
