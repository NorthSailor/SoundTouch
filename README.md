# SoundTouch

SoundTouch is a platform exploring the capabilities of the human mind for blind
users, by allowing them to use their mouse through sound feedback.

It is a fullscreen application that captures the mouse-pointer, making sounds as
it moves.

## Building & Installation

SoundTouch can be built like any other Qt 5 application. The only caveat is that
you also need Qt 5 Multimedia and espeak. In Ubuntu, you can type

```
sudo apt-get install qt5-default qtmultimedia5-dev espeak
```

There is no official installer or package yet.
