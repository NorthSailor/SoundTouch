#ifndef NODE_H
#define NODE_H

#include <QObject>

class QPainter;

class Node : public QObject
{
	Q_OBJECT
public:
	explicit Node(QObject *parent = 0);
	virtual ~Node();

	virtual QString description() const = 0;
	virtual QString caption() const = 0;

	virtual void onClick(Qt::MouseButton button) = 0;
signals:
public slots:
};

#endif // NODE_H
