#ifndef FILEBROWSER_H
#define FILEBROWSER_H

#include "gridlayout.h"
#include <QDir>

class Button;

class FileLayout : public GridLayout
{
	Q_OBJECT
public:
	explicit FileLayout(QString path, QObject *parent = nullptr);

signals:

public slots:
	void selectedItem(QString text);
	void quit();

protected:
	QDir _cd;
	Button *_quitButton;

	void updateEntries();
};

#endif // FILEBROWSER_H
