#ifndef OPTIONNODE_H
#define OPTIONNODE_H

#include "button.h"
#include <QVector>

class GridLayout;

class OptionNode : public Button
{
	Q_OBJECT
public:
	OptionNode(QString title, QVector<QString> options);
	virtual ~OptionNode();

	virtual QString description() const;
	virtual QString caption() const;

	virtual void onClick(Qt::MouseButton button);

	int value() const { return _value; }
	void setValue(int value) {
		_value = value;
	}

	const QVector<QString>& options() const { return _options; }

signals:
	void valueChanged(int newValue);

public slots:
	void buttonSelected(QString title);

protected:
	QVector<QString> _options;
	int _value;
	GridLayout *_gridLayout;

};

#endif // OPTIONNODE_H
