﻿#ifndef SWITCHNODE_H
#define SWITCHNODE_H

#include "node.h"

class SwitchNode : public Node
{
	Q_OBJECT
public:
	SwitchNode(QString title);

	bool isOn() const { return _isOn; }
	void setIsOn(bool value) {
		_isOn = value;
	}

	QString title() const { return _title; }
	void setTitle(QString title) {
		_title = title;
	}

	virtual QString description() const;
	virtual QString caption() const;

	virtual void onClick(Qt::MouseButton button);

signals:
	void valueChanged(bool isOn);

protected:
	QString _title;
	bool _isOn;
};

#endif // SWITCHNODE_H
