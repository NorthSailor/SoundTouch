#ifndef BUTTON_H
#define BUTTON_H

#include "node.h"

class Button : public Node
{
	Q_OBJECT
public:
	Button(QString title, QObject *parent = 0);
	virtual ~Button();

	QString title() const { return _title; }
	void setTitle(QString title) {
		_title = title;
	}

	virtual QString description() const;
	virtual QString caption() const;

	virtual void onClick(Qt::MouseButton button);

signals:
	void selected();
	void selected(QString title);

protected:
	QString _title;
};

#endif // BUTTON_H
