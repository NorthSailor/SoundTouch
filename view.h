#ifndef VIEW_H
#define VIEW_H

#include <QOpenGLWidget>
#include <QPen>
#include <QSoundEffect>
#include <QTimer>
#include <QStack>
#include "layout.h"

class View : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit View(QWidget *parent = 0);
    ~View();

signals:

private slots:
    void doSpeak();

public slots:
	void setSpeakFast(bool value);
	void setPunctuation(int value); // 0 = None, 1 = Some, 2 = All

	void setColorScheme(QColor background, QColor foreground);
public:
	static View *activeView();

	void pushLayout(Layout *layout);
	void popLayout();

    ///
    /// \brief Speaks a string using the speech synthesizer.
    /// \param text The text to be spoken.
    /// \param waitms Wait some milliseconds before starting to speak.
    ///
    void speak(QString text, int waitms = 200);

    enum SoundEffect {
        PingLow = 1,
        PingHigh = 2,
        BongLow = 3,
        BongHigh = 4,
        Ding = 5,
        LowHigh = 6,
        HighLow = 7
    };

    ///
    /// \brief Produces an audible effect.
    /// \param effect The type of the sound to be played out.
    ///
    void playSoundEffect(SoundEffect effect);

    /// Foreground pen for use with QPainter.
    QPen foregroundPen() const { return _fgPen; }
    /// Background pen for use with QPainter.
    QPen backgroundPen() const { return _bgPen; }

private:
	static View *_viewSingleton;

    void paintEvent(QPaintEvent *);

    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void wheelEvent(QWheelEvent *e);

    void keyPressEvent(QKeyEvent *e);

	Layout *topLayout();

    /// Necessary for making a sound only on first contact with the boundaries. Check mouseMoveEvent().
    QPoint _mousePos;

    /// The background color for the view.
    QColor _bgCol;

    /// The foreground color for the view.
    QColor _fgCol;

    /// Background pen for use with QPainter.
    QPen _bgPen;

    /// Foreground pen for use with QPainter.
    QPen _fgPen;

	QStack<Layout*> _layoutStack;

    QSoundEffect _bingLowSound;
    QSoundEffect _bingHighSound;

    QSoundEffect _pingLowSound;
    QSoundEffect _pingHighSound;

    QSoundEffect _dingSound;

    QSoundEffect _highlowSound;
    QSoundEffect _lowhighSound;

    QTimer _speechTimer;
    QString _textToSynth;
};

inline View* view() { return View::activeView(); }

#endif // VIEW_H
