#include "launcherlayout.h"
#include "button.h"
#include "switchnode.h"
#include "optionnode.h"
#include "filelayout.h"
#include "view.h"

LauncherLayout::LauncherLayout() :
    GridLayout(5, 1)
{
	fileBrowserButton = new Button("File Browser", this);
	connect(fileBrowserButton, SIGNAL(selected()), SLOT(startFileBrowser()));
	nodes()[0] = fileBrowserButton;

	fileLayout = nullptr;

	rateSwitch = new SwitchNode("Speak Fast");
	connect(rateSwitch, SIGNAL(valueChanged(bool)),
	        view(), SLOT(setSpeakFast(bool)));
	nodes()[1] = rateSwitch;

	QVector<QString> punctOptions;
	punctOptions.push_back("None");
	punctOptions.push_back("Some");
	punctOptions.push_back("All");

	punctOpt = new OptionNode("Punctuation", punctOptions);
	connect(punctOpt, SIGNAL(valueChanged(int)),
	        view(), SLOT(setPunctuation(int)));
	nodes()[2] = punctOpt;
	_colorSchemes.push_back({ "Yale", QColor(0, 53, 107), QColor(Qt::white) });
	_colorSchemes.push_back({ "Paper" , QColor(Qt::white), QColor(Qt::black) });
	_colorSchemes.push_back({ "Night Red", QColor(Qt::black), QColor(Qt::red) });
	_colorSchemes.push_back({ "Magenta", QColor(Qt::black), QColor(Qt::magenta) });

	QVector<QString> schemeOptions;
	Q_FOREACH(ColorScheme scheme, _colorSchemes) {
		schemeOptions.push_back(scheme.name);
	}

	schemeOpt = new OptionNode("Color Scheme", schemeOptions);
	connect(schemeOpt, SIGNAL(valueChanged(int)), SLOT(colorSchemeChanged(int)));
	nodes()[3] = schemeOpt;

	quitButton = new Button("Quit", this);
	connect(quitButton, SIGNAL(selected()), SLOT(quit()));
	nodes()[4] = quitButton;
}

void LauncherLayout::quit()
{
	view()->speak("Goodbye");
	exit(0);
}

void LauncherLayout::startFileBrowser()
{
	if (fileLayout)
		delete fileLayout;
	fileLayout = new FileLayout("/home/nsailor");
	view()->pushLayout(fileLayout);
}

void LauncherLayout::colorSchemeChanged(int value)
{
	ColorScheme scheme = _colorSchemes.at(value);
	view()->setColorScheme(scheme.background, scheme.foreground);
}
