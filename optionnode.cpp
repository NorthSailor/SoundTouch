#include "optionnode.h"
#include "gridlayout.h"
#include "view.h"
#include "button.h"
#include <QDebug>

OptionNode::OptionNode(QString title,
                       QVector<QString> options) :
    Button(title),
    _options(options),
    _value(0)
{
	Q_ASSERT(options.size() > 0);
	_gridLayout = new GridLayout(options.size(), 1);
	int optionIndex = 0;
	Q_FOREACH(QString optionName, options) {
		Button *button = new Button(optionName, this);
		connect(button, SIGNAL(selected(QString)),
		        SLOT(buttonSelected(QString)));
		_gridLayout->nodes()[optionIndex] = button;
		optionIndex++;
	}
}

OptionNode::~OptionNode()
{
	delete _gridLayout;
}

QString OptionNode::description() const
{
	return "Option node.";
}

QString OptionNode::caption() const
{
	return title() + " - " + _options.at(_value);
}

void OptionNode::onClick(Qt::MouseButton button)
{
	view()->pushLayout(_gridLayout);
	view()->update();
}

void OptionNode::buttonSelected(QString title)
{
	// Search for the right item
	// Not the prettiest solution but we need that index!
	for (int i = 0; i < _options.size(); i++) {
		if (_options.at(i) == title) {
			_value = i;
			emit valueChanged(_value);
		}
	}
	view()->popLayout();
	view()->update();
}
