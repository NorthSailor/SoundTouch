#include "button.h"
#include <QDebug>

Button::Button(QString title, QObject *parent) :
    Node(parent),
    _title(title)
{
}

Button::~Button() { }

QString Button::description() const
{
	return _title + " - Button";
}

QString Button::caption() const
{
	return _title;
}

void Button::onClick(Qt::MouseButton button)
{
	Q_UNUSED(button);
	emit selected();
	emit selected(title());
}
