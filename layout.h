#ifndef LAYOUT_H
#define LAYOUT_H

#include <QObject>
#include <QMouseEvent>

class Layout : public QObject
{
    Q_OBJECT
public:

signals:

public slots:
    virtual void paint(QPainter *p) = 0;
    virtual void handleMouseEvent(QMouseEvent *e) = 0;
    virtual void handleKeyEvent(QKeyEvent *e) = 0;
    virtual void handleWheelEvent(QWheelEvent *e) = 0;
};

#endif // LAYOUT_H
