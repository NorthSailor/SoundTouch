#include "gridlayout.h"
#include "view.h"
#include "node.h"

#include <QDebug>
#include <QPainter>
#include <QMouseEvent>
#include <QWheelEvent>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

GridLayout::GridLayout(int rows, int columns) :
    _rows(rows),
    _columns(columns),
    _rectWidth(0), _rectHeight(0)
{
	Q_ASSERT(_rows > 0);
	Q_ASSERT(_columns > 0);

	_activeRow = 0;
	_activeColumn = 0;

	nodes().resize(rows * columns);

	view()->update();
}

void GridLayout::moveTo(int row, int column)
{
	Q_ASSERT((row >= 0) && (row < _rows));
	Q_ASSERT((column >= 0) && (column < _columns));

	if (_activeRow != row)
		view()->playSoundEffect(View::PingHigh);

	if (_activeColumn != column)
		view()->playSoundEffect(View::PingLow);

	_activeRow = row;
	_activeColumn = column;

	view()->update();

	// Announce the new item.
	view()->speak(textAt(row, column));
}

QString GridLayout::textAt(int row, int column)
{
	Q_ASSERT((row >= 0) && (row < _rows));
	Q_ASSERT((column >= 0) && (column < _columns));

	Node *node = nodeAt(row, column);
	if (node == nullptr) {
		return "Null node";
	} else {
		return node->caption();
	}
}

Node *GridLayout::nodeAt(int row, int column)
{
	Q_ASSERT((row >= 0) && (row < _rows));
	Q_ASSERT((column >= 0) && (column < _columns));

	return nodes().at(row * _columns + column);
}

int clamp_int(int x, int a, int b)
{
	if (x < a)
		return a;
	if (x > b)
		return b;
	return x;
}

void GridLayout::handleMouseEvent(QMouseEvent *e)
{
	switch (e->type())
	{
	case QMouseEvent::MouseMove:
	{
		if (_rectWidth == 0) return;
		int column = e->pos().x() / _rectWidth;
		int row = e->pos().y() / _rectHeight;
		if ((_activeRow != row) || (_activeColumn != column)) {
			int newColumn = clamp_int(column, 0, _columns - 1);
			int newRow = clamp_int(row, 0, _rows - 1);
			moveTo(newRow, newColumn);
		}
		break;
	}
	case QMouseEvent::MouseButtonRelease:
	{
		Node *selectedNode = nodeAt(_activeRow, _activeColumn);
		if (selectedNode == nullptr)
			return;
		selectedNode->onClick(e->button());
		break;
	}
	default:
		break;
	}
}

void GridLayout::handleKeyEvent(QKeyEvent *e)
{
	if ((e->type() != QKeyEvent::KeyPress) || (e->isAutoRepeat()))
		return;

	switch (e->key())
	{
	case Qt::Key_Up:
		if (_activeRow)
			moveTo(_activeRow - 1, _activeColumn);
		else
			view()->playSoundEffect(View::BongHigh);
		break;
	case Qt::Key_Down:
		if (_activeRow < (_rows - 1))
			moveTo(_activeRow + 1, _activeColumn);
		else
			view()->playSoundEffect(View::BongHigh);
		break;
	case Qt::Key_Left:
		if (_activeColumn)
			moveTo(_activeRow, _activeColumn - 1);
		else
			view()->playSoundEffect(View::BongLow);
		break;
	case Qt::Key_Right:
		if (_activeColumn < (_columns - 1))
			moveTo(_activeRow, _activeColumn + 1);
		else
			view()->playSoundEffect(View::BongLow);
		break;
	default:
		break;
	}
}

void GridLayout::handleWheelEvent(QWheelEvent *e)
{
	Q_UNUSED(e)
}

void GridLayout::paint(QPainter *p)
{
	// Draw the grid

	// Distances between splitting lines (updated at every paint cycle)
	_rectWidth = view()->width() / _columns;
	_rectHeight = view()->height() / _rows;

	/// @todo Optimize this part.
	for (int i = 1; i < _columns; i++)
	{
		p->drawLine(_rectWidth * i, 0, _rectWidth * i, view()->height());
	}

	for (int i = 1; i < _rows; i++)
	{
		p->drawLine(0, _rectHeight * i, view()->width(), _rectHeight * i);
	}

	// Fill the active rectangle
	p->fillRect(_activeColumn * _rectWidth, _activeRow * _rectHeight,
	            _rectWidth, _rectHeight, view()->foregroundPen().color());

	// Draw the labels
	p->setFont(QFont("Verdana", MIN(_rectHeight / 8, _rectWidth / 16)));
	for (int column = 0; column < _columns; column++)
	{
		for (int row = 0; row < _rows; row++)
		{
			QRect rect(column * _rectWidth, row * _rectHeight, _rectWidth, _rectHeight);
			if ((row == _activeRow) && (column == _activeColumn))
				p->setPen(view()->backgroundPen());
			else
				p->setPen(view()->foregroundPen());
			p->drawText(rect, Qt::AlignCenter, textAt(row, column));
		}
	}
}

void GridLayout::resize(int rows, int columns)
{
	Q_ASSERT(rows > 0);
	Q_ASSERT(columns > 0);

	_nodes.resize(rows * columns);
	for (int i = _rows * _columns; i < _nodes.size(); i++)
		_nodes[i] = nullptr;

	_rows = rows;
	_columns = columns;

	moveTo(0, 0);

	view()->update();
}
