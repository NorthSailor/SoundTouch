#include "filelayout.h"
#include "button.h"
#include "view.h"
#include <QDebug>

FileLayout::FileLayout(QString path,
                       QObject *parent) :
    GridLayout(1, 1),
    _cd(path)
{
	_quitButton = new Button("Exit");
	nodes()[0] = _quitButton;
	connect(_quitButton, SIGNAL(selected()), SLOT(quit()));
	updateEntries();
}

void FileLayout::selectedItem(QString text)
{
	QString name = text;
	if (name == "Back") {
		_cd.cdUp();
	}

	QString newPath = _cd.absolutePath() + "/" + name;
	_cd.cd(text);
	updateEntries();
	view()->update();
}

void FileLayout::updateEntries()
{
	QFileInfoList entries = _cd.entryInfoList(QStringList("*"));
	int rows = (int)(((float)(entries.size() + 1) / 5.0f) + 0.5f);
	if (rows == 0)
		rows = 1;
	int columns = 5;
	resize(rows, columns);

	int i = 1;
	Q_FOREACH(QFileInfo fi, entries) {
		QString label = "";
		if (fi.fileName() == ".")
			continue;
		if (fi.fileName() == "..")
			label = "Back";
		else
			label = fi.completeBaseName();

		Button *button = new Button(label);
		connect(button, SIGNAL(selected(QString)), SLOT(selectedItem(QString)));
		nodes()[i] = button;
		i++;
	}
}

void FileLayout::quit()
{
	view()->popLayout();
}
