#include "view.h"
#include <QDebug>
#include <QPainter>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QWheelEvent>

#ifdef USE_ESPEAK
#include <espeak/speak_lib.h>

int DummySynthCallback(short*, int, espeak_EVENT*)
{
    return 0; // Continue synthesis - 1 for abort
}
#endif

View *View::_viewSingleton = nullptr;

View::View(QWidget *parent) : QOpenGLWidget(parent)
{
    setWindowTitle("SoundTouch View");
    speak("Welcome to SoundTouch");
    setMouseTracking(true);

	_viewSingleton = this;

#ifdef USE_ESPEAK
    qDebug() << "Initializing speech...";
    espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, NULL, 0);
    espeak_SetSynthCallback(DummySynthCallback);

    espeak_SetParameter(espeakVOLUME, 70, 0);
    qDebug() << "Espeak Version : " << espeak_Info(NULL);
#else
    qDebug() << "Espeak support not enabled during build. Speech output disabled.";
#endif

    connect(&_speechTimer, SIGNAL(timeout()), this, SLOT(doSpeak()));
    _speechTimer.setSingleShot(true);

    _bgCol = QColor(0, 53, 107); // Yale blue
    // _bgCol = QColor(196, 30, 58); // Cardinal red
    _fgCol = QColor(Qt::white);

    _bgPen.setColor(_bgCol);
    _fgPen.setColor(_fgCol);

    qDebug() << "Loading sound effects...";

    _bingLowSound.setSource(QUrl::fromLocalFile("bong-low.wav"));
    _bingHighSound.setSource(QUrl::fromLocalFile("bong-high.wav"));

    _pingLowSound.setSource(QUrl::fromLocalFile("ping-low.wav"));
    _pingHighSound.setSource(QUrl::fromLocalFile("ping-high.wav"));

    _dingSound.setSource(QUrl::fromLocalFile("pluck-high.wav"));

    _lowhighSound.setSource(QUrl::fromLocalFile("low-high.wav"));
    _lowhighSound.setVolume(.7f);
    _highlowSound.setSource(QUrl::fromLocalFile("high-low.wav"));
    _highlowSound.setVolume(.7f);

    qDebug() << "Initialization complete.";
}

View::~View()
{
#ifdef USE_ESPEAK
    espeak_Terminate();
#endif
}

View *View::activeView()
{
	return _viewSingleton;
}

void View::pushLayout(Layout *layout)
{
	speak("New layout");
	_layoutStack.push(layout);
}

void View::popLayout()
{
	speak("Previous layout");
	_layoutStack.pop();
}

void View::speak(QString text, int waitms)
{
    _speechTimer.stop();
    _textToSynth = text;
    _speechTimer.setInterval(waitms);
    _speechTimer.start();
}

void View::setSpeakFast(bool value)
{
#ifdef USE_ESPEAK
	espeak_Cancel();
	espeak_SetParameter(espeakRATE,
	                    value ? 400 : 150, 0);
#endif
}

void View::setPunctuation(int value)
{
#ifdef USE_ESPEAK
	espeak_Cancel();
	int punct = 0;
	switch (value) {
	case 0:
		punct = espeakPUNCT_NONE;
		break;
	case 1:
		punct = espeakPUNCT_SOME;
		break;
	case 2:
		punct = espeakPUNCT_ALL;
		break;
	default:
		qDebug() << "Asked to set invalid punctuation level.";
	}

	espeak_SetParameter(espeakPUNCTUATION, punct, 0);
#endif
}

void View::setColorScheme(QColor background, QColor foreground)
{
	_fgPen.setColor(foreground);
	_bgPen.setColor(background);
	update();
}

void View::doSpeak()
{
#ifdef USE_ESPEAK
    espeak_Cancel();
    // Better not supply an empty string to e-speak.
    if (_textToSynth.length() == 0)
        return;
    QByteArray _textUtf8 = _textToSynth.toUtf8();
    espeak_Synth(_textUtf8.constData(), _textUtf8.size() * 2, 0, POS_CHARACTER,
                 0, espeakCHARS_AUTO,
                 NULL, NULL);
#else
	qDebug() << "Speech output is disabled. String : \"" << _textToSynth << "\"";
#endif
}

void View::playSoundEffect(SoundEffect effect)
{
    switch (effect)
    {
    case PingLow:
        _pingLowSound.play();
        break;
    case PingHigh:
        _pingHighSound.play();
        break;
    case BongLow:
        _bingLowSound.play();
        break;
    case BongHigh:
        _bingHighSound.play();
        break;
    case Ding:
        _dingSound.play();
        break;
    case LowHigh:
        _lowhighSound.play();
        break;
    case HighLow:
        _highlowSound.play();
        break;
    default:
        qDebug() << "Requested to play unknown sound effect! - Ignoring.";
        break;
    }
}

void View::paintEvent(QPaintEvent *)
{
    QPainter p(this);
	p.fillRect(0, 0, width(), height(), _bgPen.color());
    p.setPen(_fgPen);

	if (topLayout())
		topLayout()->paint(&p);
}

void View::mouseMoveEvent(QMouseEvent *e)
{
    static bool wasNearAnEdgeX = false; // Otherwise, a fart sound would come out. :)
    static bool wasNearAnEdgeY = false;

	if (topLayout())
		topLayout()->handleMouseEvent(e);

    // Do a bounds check.
    if ((_mousePos.x() <= 1 || _mousePos.x() >= width() - 1))
    {
        if (!wasNearAnEdgeX)
            _bingLowSound.play();
        wasNearAnEdgeX = true;
    } else {
        wasNearAnEdgeX = false;
    }

    if (_mousePos.y() <= 1 || _mousePos.y() >= height() - 1)
    {
        if (!wasNearAnEdgeY)
            _bingHighSound.play();
        wasNearAnEdgeY = true;
    } else {
        wasNearAnEdgeY = false;
    }

    _mousePos = e->pos();

    // Passing the responsibility for scheduling repaints to the layouts also means
    // that if they are buggy the whole view will freeze. Not that it would be much
    // use without a functional layout though. On the other hand, the performance gain
    // is huge.
    // In case of instabilities, uncomment the following call.
    // update();
}

void View::mouseReleaseEvent(QMouseEvent *e)
{
	if (topLayout())
		topLayout()->handleMouseEvent(e);
}

void View::keyPressEvent(QKeyEvent *e)
{
	if (topLayout())
		topLayout()->handleKeyEvent(e);
}

void View::wheelEvent(QWheelEvent *e)
{
	if (topLayout())
		topLayout()->handleWheelEvent(e);
}

Layout *View::topLayout()
{
	if (_layoutStack.isEmpty()) {
		return nullptr;
	} else {
		return _layoutStack.top();
	}
}
