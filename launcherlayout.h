#ifndef LAUNCHERLAYOUT_H
#define LAUNCHERLAYOUT_H

#include "gridlayout.h"
#include <QColor>

class Button;
class SwitchNode;
class OptionNode;
class FileLayout;

struct ColorScheme {
	QString name;
	QColor background;
	QColor foreground;
};

class LauncherLayout : public GridLayout
{
	Q_OBJECT
public:
	LauncherLayout();

public slots:
	void quit();
	void startFileBrowser();
	void colorSchemeChanged(int value);

protected:
	Button *quitButton;
	SwitchNode *rateSwitch;
	OptionNode *punctOpt;
	OptionNode *schemeOpt;
	Button *fileBrowserButton;
	FileLayout *fileLayout;
	QVector<ColorScheme> _colorSchemes;

};

#endif // LAUNCHERLAYOUT_H
