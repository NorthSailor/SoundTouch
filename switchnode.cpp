#include "switchnode.h"
#include "view.h"
#include <QDebug>

SwitchNode::SwitchNode(QString title) :
    _title(title),
    _isOn(false)
{
}

QString SwitchNode::description() const
{
	return "Switch. Select to toggle.";
}

QString SwitchNode::caption() const
{
	return title() + (_isOn ? (" - On") : (" - Off"));
}

void SwitchNode::onClick(Qt::MouseButton button)
{
	Q_UNUSED(button);
	_isOn = !_isOn;
	emit valueChanged(_isOn);
	if (_isOn)
		view()->speak("On");
	else
		view()->speak("Off");
	view()->update();
}
