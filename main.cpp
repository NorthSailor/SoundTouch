#include "view.h"
#include <QApplication>
#include "gridlayout.h"
#include "button.h"
#include "switchnode.h"
#include "optionnode.h"
#include "launcherlayout.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	View view;
	view.setSpeakFast(false);
	view.showFullScreen();

	LauncherLayout launcher;
	view.pushLayout(&launcher);

	return a.exec();
}
